# Copyright (c) 2019-2020 ZCaliptium.
extends Object
class_name GDDlg_DialogDefinition

# Fields.
var identifier: String;
var attributes: Dictionary = {};
var options: Array = [];

# Constructor.
func _init(id: String) -> void:
	identifier = id;

func get_attribute(key: String, default = null):
	return attributes.get(key, default);

# Returns Dictionary that represents this dialog definition.
#   Use to_json on result to get JSON string.
func to_data() -> Dictionary:
	var data: Dictionary = {};
	
	data["id"] = identifier;
	data["attributes"] = attributes;
	data["options"] = options;
	
	return data;
